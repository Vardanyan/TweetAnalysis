package core;

/**
 * Created by nane on 3/15/17.
 */
public interface Completion<T> {
    void onCompleted(T result);
}
