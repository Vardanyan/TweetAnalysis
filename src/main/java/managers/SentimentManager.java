package managers;

import com.vdurmont.emoji.Emoji;
import core.FileConstants;
import edu.stanford.nlp.dcoref.Dictionaries;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.tokensregex.CoreMapNodePattern;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.StreamGobbler;
import utils.FileUtils;

import java.io.File;
import java.util.*;

/**
 * Created by nane on 3/16/17.
 */
public class SentimentManager {
    private  StanfordCoreNLP pipeline;
    private static SentimentManager ourInstance = new SentimentManager();

    public static SentimentManager getInstance() {
        return ourInstance;
    }

    private SentimentManager() {
    }

    public StanfordCoreNLP getPipeline() {
        return pipeline;
    }

    public void initialise(){
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma," +
                " ner, parse, dcoref, sentiment");

        pipeline = new StanfordCoreNLP(props);
    }

    public String evaluate( String text) {
        Annotation annotation = new Annotation(text);
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        if (sentences != null && !sentences.isEmpty()) {
            CoreMap sentence = sentences.get(0);
            return sentence.get(SentimentCoreAnnotations.SentimentClass.class);
        }
        return null;
    }
}

