package managers;

import core.Completion;
import core.FileConstants;
import dataModel.Tweet;
import factories.TweetFactory;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TwitterManager {
    public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

    private static TwitterManager instance = new TwitterManager();

    private TwitterManager() {
    }

    public static TwitterManager getInstance() {
        return instance;
    }

    public void LoadTweets(Completion<JSONArray> completion) {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("x0vKFRV7ZyqDG8sxl4bhzt2tD")
                .setOAuthConsumerSecret("8lhULUtUehcv3dRuM3kpEPv59C2TWvwR9HlCvnWiBjLBf2jCHu")
                .setOAuthAccessToken("828701599291805696-HnNrFC5qe4o0fBfDaqoubXjy0Sv0ykt")
                .setOAuthAccessTokenSecret("svsDuZa6Jy3WLc5MLpBnaRhPpixWJrLS9Nzi8Ty0GvEV2");

        Twitter twitter = new TwitterFactory(cb.build()).getInstance();
        Query query = new Query("rome +exclude:retweets");
        int numberOfTweets = 300;
        long lastID = Long.MAX_VALUE;
        List<Status> tweets = new ArrayList<Status>();
        while (tweets.size() < numberOfTweets) {
            if (numberOfTweets - tweets.size() > 100)
                query.setCount(100);
            else
                query.setCount(numberOfTweets - tweets.size());
            try {
                query.setLang("en");
                QueryResult result = twitter.search(query);
                tweets.addAll(result.getTweets());
                for (int i = 0; i < tweets.size(); i++) {
                    if (tweets.get(i).getUser().getName().toUpperCase().contains("ROME")) {
                        System.out.println("Removing tweet " + tweets.get(i).getUser().getName());
                        tweets.remove(i);
                    }
                }

//                Pattern p = Pattern.compile(URL_REGEX);
//                for (int i = 0; i < tweets.size(); i++) {
//                    Matcher m = p.matcher(tweets.get(i).getText());
//                    if(!m.find()) {
//                        System.out.println("Removing---------- " + tweets.get(i).getText());
//                        tweets.remove(i);
//                    }
//                }

                System.out.println("Gathered " + tweets.size() + " tweets");
                for (Status t : tweets)
                    if (t.getId() < lastID) lastID = t.getId();

            } catch (TwitterException te) {
                System.out.println("Couldn't connect: " + te);
            }
            query.setMaxId(lastID - 1);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(tweets.size());
        JSONArray jsonArray = new JSONArray();
        for (Status status : tweets) {
            Tweet tweet = TweetFactory.createTweet(status);
            JSONObject jsonObject = TweetFactory.getJSON(tweet);
            jsonArray.put(jsonObject);
        }
        completion.onCompleted(jsonArray);

    }
}