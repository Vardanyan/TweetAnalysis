package core;

/**
 * Created by nane on 4/20/17.
 */
public interface Evaluator {
    double evaluate(String text);
}
