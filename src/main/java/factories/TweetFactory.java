package factories;

import dataModel.Tweet;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import twitter4j.Status;

import java.util.Date;


/**
 * Created by nane on 3/14/17.
 */
public class TweetFactory {

    public static Tweet createTweet(Status status) {
        Tweet tweet = new Tweet();

        tweet.setAuthor(status.getUser().getScreenName());
        tweet.setText(status.getText());
        tweet.setDate(status.getCreatedAt());

        return tweet;
    }

    public static JSONObject getJSON(Tweet tweet){
        JSONObject json = new JSONObject();
        try {
            json.put("author", tweet.getAuthor());
            json.put("text", tweet.getText());
            json.put("location", tweet.getLocation());
            json.put("date", tweet.getDate());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static Tweet getTweet(JSONObject jsonObject){
        Tweet tweet = new Tweet();
        tweet.fromJson(jsonObject);
        return tweet;
    }

    public static Tweet getTweet(JSONObject jsonObject, boolean flag){
        Tweet tweet = new Tweet();
        tweet.getTweetFromJson(jsonObject);
        return tweet;
    }
}
