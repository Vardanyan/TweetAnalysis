package utils;

import dataModel.Tweet;
import factories.TweetFactory;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nane on 3/15/17.
 */
public class FileUtils {

    public static boolean writeJSonArrayToFile(JSONArray jsonArray, String fileName) {
        if (jsonArray == null || StringUtils.isEmpty(fileName)) {
            return false;
        }

        FileWriter fileWriter;
        BufferedWriter bufferedWriter;
        try {
            fileWriter = new FileWriter(fileName);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        try {
            bufferedWriter.write(jsonArray.toString());
            bufferedWriter.flush();
            bufferedWriter.close();
            System.out.println(jsonArray);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }



    public static boolean writeOverallSentimentsToFile(JSONArray jsonArray, String fileName) {
        if (jsonArray == null || StringUtils.isEmpty(fileName)) {
            return false;
        }
        FileWriter fileWriter;
        BufferedWriter bufferedWriter;
        try {
            fileWriter = new FileWriter(fileName);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject current = (JSONObject) jsonArray.get(i);
                Tweet tweet = TweetFactory.getTweet(current, true);
                bufferedWriter.write(String.valueOf(tweet.getOverallSentiment()));
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public static Map loadMyMapFromFile(String fileName) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            Map map = new HashMap();
            String line = bufferedReader.readLine();
            while (line != null) {
                map.put(line.charAt(0), Integer.parseInt(line.substring(2, 3)));
                //sb.append("\n");
                line = bufferedReader.readLine();
            }
            return map;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String loadStringFromFile(String fileName) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            StringBuilder sb = new StringBuilder();
            String line = bufferedReader.readLine();
            while (line != null) {
                sb.append(line);
                //sb.append("\n");
                line = bufferedReader.readLine();
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static JSONArray loadJsonArrayFromFile(String fileName) {
        String data = loadStringFromFile(fileName);
        if (data != null) {
            try {
                JSONArray jsonArray = new JSONArray(data);
                return jsonArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static Map<String, Integer> loadEmojiSentimentMapFromFile(String fileName) {
        Map<String, Integer> emojiSentimentMap = new HashMap<String, Integer>();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            String line = bufferedReader.readLine();
            int index;
            String emoji;
            Integer sentiment;
            while (!StringUtils.isEmpty(line)) {
                index = line.indexOf(":");
                emoji = line.substring(0, index);
                sentiment = Integer.parseInt(line.substring(index + 1, line.length()).trim());
                emojiSentimentMap.put(emoji, sentiment);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return emojiSentimentMap;
    }

    public static Map<String, String> loadSlangDictionaryFromFile(String fileName) {
        Map<String, String> slangDictionary = new HashMap<String, String>();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            String line = bufferedReader.readLine();
            int index;
            String slangWord;
            String word;
            while (!StringUtils.isEmpty(line)) {
                index = line.indexOf(":");
                slangWord = line.substring(0, index);
                word = line.substring(index + 1, line.length()).trim();
                slangDictionary.put(slangWord, word);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return slangDictionary;
    }
}