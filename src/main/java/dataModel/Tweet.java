package dataModel;

import core.Evaluator;
import interfaces.Serializable;
import twitter4j.GeoLocation;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import utils.StringUtils;

import java.util.Date;

/**
 * Created by nane on 3/15/17.
 */
public class Tweet implements Serializable {
    private String author;
    private String text;
    private GeoLocation location;
    private Date date;
    private String sentiment;
    private double textSentiment;
    private double hasTaggedWordsSentiment;
    private double emojiSentiment;
    private double overallSentiment;

    public double getHasTaggedWordsSentiment() {
        return hasTaggedWordsSentiment;
    }

    public void setHasTaggedWordsSentiment(double hasTaggedWordsSentiment) {
        this.hasTaggedWordsSentiment = hasTaggedWordsSentiment;
    }

    public double getEmojiSentiment() {
        return emojiSentiment;
    }

    public double getOverallSentiment() {
        return overallSentiment;
    }

    public void setOverallSentiment(double overallSentiment) {
        this.overallSentiment = overallSentiment;
    }

    public void setEmojiSentiment(double emojiSentiment) {
        this.emojiSentiment = emojiSentiment;
    }

    public double getTextSentiment() {
        return textSentiment;
    }

    public void setTextSentiment(double textSentiment) {
        this.textSentiment = textSentiment;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            if (!StringUtils.isEmpty(author)) {
                json.put("author", author);
            }
            if (!StringUtils.isEmpty(author)) {
                json.put("text", text);
            }
            if (!StringUtils.isEmpty(author)) {
                json.put("sentiment", sentiment);
            }
            json.put("textSentiment", textSentiment);
            json.put("emojiSentiment", emojiSentiment);
            json.put("overallSentiment", overallSentiment);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public void fromJson(JSONObject jsonObject) {
        try {
            this.author = jsonObject.getString("author");
            this.text = jsonObject.getString("text");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getTweetFromJson(JSONObject jsonObject) {
        try {
            this.author = jsonObject.getString("author");
            this.text = jsonObject.getString("text");
            this.overallSentiment = Double.valueOf(jsonObject.getString("overallSentiment"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public double evaluate(Evaluator evaluator) {
        return evaluator.evaluate(this.text);
    }
}
