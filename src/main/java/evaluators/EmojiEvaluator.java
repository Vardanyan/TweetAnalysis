package evaluators;

import com.vdurmont.emoji.Emoji;
import core.Evaluator;
import core.FileConstants;
import utils.FileUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by nane on 4/24/17.
 */
public class EmojiEvaluator implements Evaluator {

    Map<String, Integer> emojiSentimentMap;

    public EmojiEvaluator() {
        this.emojiSentimentMap =
                FileUtils.loadEmojiSentimentMapFromFile(FileConstants.EMOJI_FILE_NAME);
    }

    public double evaluate(String text) {
        return getSentimentOfEmojiesFromText(text, emojiSentimentMap);
    }

    private Double getSentimentOfEmojiesFromText(String text, Map<String, Integer> emojiSentimentMap) {
        List<Emoji> emojies = getEmojiesFromText(text);
        Double result = getSentimentsFromEmoji(emojies, emojiSentimentMap);
        return result;
    }

    private List<Emoji> getEmojiesFromText(String text) {

        Collection<Emoji> emojiList = com.vdurmont.emoji.EmojiManager.getAll();
        List<Emoji> containedEmojis = new ArrayList();
        for (Emoji emoji : emojiList) {
            if (text.contains(emoji.getUnicode())) {
                containedEmojis.add(emoji);
            }
        }
        return containedEmojis;
    }

    private Double getSentimentsFromEmoji(List<Emoji> emojiesFromText,
                                           Map<String, Integer> mapOfSentiments) {
        Double result = 0.0;
        List<Integer> sentiments = new ArrayList<Integer>();
        for (Emoji emoji : emojiesFromText) {
            for (String emojiWithSentiment : mapOfSentiments.keySet()) {
                if (emoji.getAliases().contains(emojiWithSentiment)) {
                    sentiments.add(mapOfSentiments.get(emojiWithSentiment));
                }
            }
        }
        for (int i = 0; i < sentiments.size(); i++) {
            result += sentiments.get(i);
        }
        if (sentiments.size() == 0) {
            return 0.0;
        }
        return result / sentiments.size();
    }
}
