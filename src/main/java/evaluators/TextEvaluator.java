package evaluators;

import core.Evaluator;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import managers.SentimentManager;

import java.util.List;

/**
 * Created by nane on 4/24/17.
 */
public class TextEvaluator implements Evaluator {

    public double evaluate(String text) {
        StanfordCoreNLP pipeline = SentimentManager.getInstance().getPipeline();
        Annotation annotation = new Annotation(text);
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        if (sentences != null && !sentences.isEmpty()) {
            CoreMap sentence = sentences.get(0);
            String sentiment = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
            if (sentiment.toUpperCase().equals("NEGATIVE")){
                return -0.5;
            }else if (sentiment.toUpperCase().equals("VERY NEGATIVE")){
                return -1.0;
            }else if (sentiment.toUpperCase().equals("POSITIVE")){
                return 0.5;
            }else if (sentiment.toUpperCase().equals("VERY POSITIVE")){
                return 1.0;
            }else if (sentiment.toUpperCase().equals("NEUTRAL")){
                return 0.0;
            }
        }
        return 0;
    }


}
