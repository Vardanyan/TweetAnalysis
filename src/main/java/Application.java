import core.Completion;
import core.FileConstants;
import dataModel.Tweet;
import evaluators.EmojiEvaluator;
import evaluators.HashTagWordsEvaluator;
import evaluators.TextEvaluator;
import factories.TweetFactory;
import managers.*;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import utils.FileUtils;
import utils.StringUtils;

public class Application {

    public static void main(String[] args) {

        final String fileName = FileConstants.TWEET_FILE_NAME;

        SentimentManager.getInstance().initialise();
/**
 * get Tweets from Twitter and write into a file
 */

        TwitterManager.getInstance().LoadTweets(new Completion<JSONArray>() {
            public void onCompleted(JSONArray result) {
                if (result != null) {
                    FileUtils.writeJSonArrayToFile(result, fileName);
                }
            }
        });
/**
 * read tweets from file and calculate the sentiment
 */

        JSONArray jsonArray = FileUtils.loadJsonArrayFromFile(fileName);
        JSONArray tweetsWithSentiment = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject current = (JSONObject) jsonArray.get(i);
                Tweet tweet = TweetFactory.getTweet(current);
                tweet.setHasTaggedWordsSentiment(tweet.evaluate(new HashTagWordsEvaluator()));
                String text = tweet.getText();
                text = StringUtils.removeUrl(text);
                //text = SlangDetector.detectAndReplaceSlangWordsInText(text);
                text = StringUtils.removeHashTags(text);
                tweet.setText(text);
                String sentiment = SentimentManager.getInstance().evaluate(text);
                tweet.setSentiment(sentiment);
                tweet.setTextSentiment(tweet.evaluate(new TextEvaluator()));
                tweet.setEmojiSentiment(tweet.evaluate(new EmojiEvaluator()));
                tweet.setOverallSentiment(new OverallSentiment().calculateOverallSentiment(tweet));
                tweetsWithSentiment.put(tweet.toJson());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        FileUtils.writeOverallSentimentsToFile(tweetsWithSentiment, FileConstants.OVERALL_SENTIMENT);
        FileUtils.writeJSonArrayToFile(tweetsWithSentiment, FileConstants.SENTIMENTS_FILE_NAME);

    }
}











