package interfaces;

import twitter4j.JSONObject;

/**
 * Created by nane on 3/17/17.
 */
public interface Serializable {
    public JSONObject toJson();
    public void fromJson(JSONObject jsonObject);
}
