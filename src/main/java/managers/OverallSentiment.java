package managers;

import dataModel.Tweet;

/**
 * Created by nane on 5/7/17.
 */
public class OverallSentiment {

    public double calculateOverallSentiment(Tweet tweet) {
        double result = 0;
        if (!Double.isNaN(tweet.getHasTaggedWordsSentiment())){
            result = (tweet.getTextSentiment() + tweet.getHasTaggedWordsSentiment() / 2);
        }else {
            result = tweet.getTextSentiment();
        }
        if (tweet.getEmojiSentiment() != 0){
            result = (result + tweet.getEmojiSentiment()) / 2;
        }
        return result;
    }
}
