package core;

import utils.StringUtils;

/**
 * Created by nane on 3/21/17.
 */
public class FileConstants {
    public static final String TWEET_FILE_NAME = "tweets.txt";
    public static final String SENTIMENTS_FILE_NAME = "sentiments.txt";
    public static final String EMOJI_FILE_NAME = "emojiSentiment.txt";
    public static final String SLANG_DICTIONARY_FILE_NAME = "SlangDictionary.txt";
    public static final String OVERALL_SENTIMENT = "overallSentiment.txt";
    public static final String SENTIMENTS = "OnlyTextSentiments.txt";
    public static final String EMOJI_SENTIMENTS = "OnlyEmojiSentiments.txt";
}
