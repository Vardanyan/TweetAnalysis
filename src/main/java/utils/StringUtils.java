package utils;

import javafx.scene.web.WebHistory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nane on 3/15/17.
 */
public class StringUtils {

    public static boolean isEmpty(String text){
        if(text == null || text.length() == 0){
            return true;
        }
        return false;
    }

    public static String removeHashTags(String text){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char current = text.charAt(i);
            if ( current != '#'){
                sb.append(current);
            }else {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    public static String removeAnnotations(String text){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char current = text.charAt(i);
            if ( current != '@'){
                sb.append(current);
            }else {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    public static String removeUrl(String commentstr)
    {
        String urlPattern = "((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern p = Pattern.compile(urlPattern,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(commentstr);
        int i = 0;
        while (m.find()) {
            commentstr = commentstr.replace(m.group(i),"").trim();
            i++;
        }
        return commentstr;
    }
}
