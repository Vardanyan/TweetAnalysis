package managers;

import core.FileConstants;
import utils.FileUtils;

import java.util.Map;

/**
 * Created by argishty on 4/14/17.
 */
public class SlangDetector {

    static Map<String, String> slangDictionary = FileUtils.loadSlangDictionaryFromFile(
            FileConstants.SLANG_DICTIONARY_FILE_NAME);

    public static String detectAndReplaceSlangWordsInText(String text) {
        text = text.toUpperCase();

        for (String key : slangDictionary.keySet()) {
            if (text.contains(" " + key + " ")){
                text = text.replace( key, slangDictionary.get(key));
            }
        }
        return text;
    }

}
