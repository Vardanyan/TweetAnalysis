package evaluators;

import core.Evaluator;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import managers.SentimentManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nane on 5/4/17.
 */
public class HashTagWordsEvaluator implements Evaluator{

    public double evaluate(String text) {
        double result = 0;
        double length = 0;
        List<Double> hashTaggedWordsSentiments = getSentimentsOfHashTaggedWords(text);
        for (Double sentiment: hashTaggedWordsSentiments){
            if (sentiment != 0){
                result += sentiment;
                length++;
            }
        }
        return result/length;
    }

    private static List<String> getHashTagWordsList(String text){
        text = text + " ";
        List<String> hashTagWords = new ArrayList<String>();
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '#'){
                int start = i + 1;
                while (text.charAt(i) != ' '){
                    i++;
                }
                hashTagWords.add(text.substring(start, i));
            }
        }

        return hashTagWords;
    }

    private static List<Double> getSentimentsOfHashTaggedWords(String text){
        List<String> words = getHashTagWordsList(text);
        List<Double>  hashTaggedWordsSentiments = new ArrayList<Double>();

        for (String word: words) {
            StanfordCoreNLP pipeline = SentimentManager.getInstance().getPipeline();
            Annotation annotation = new Annotation(word);
            pipeline.annotate(annotation);
            List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);

            if (sentences != null && !sentences.isEmpty()) {
                CoreMap sentence = sentences.get(0);
                String sentiment = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
                if (sentiment.toUpperCase().equals("NEGATIVE")){
                    hashTaggedWordsSentiments.add(-0.5);
                }else if (sentiment.toUpperCase().equals("VERY NEGATIVE")){
                    hashTaggedWordsSentiments.add(-1.0);
                }else if (sentiment.toUpperCase().equals("POSITIVE")){
                    hashTaggedWordsSentiments.add(0.5);
                }else if (sentiment.toUpperCase().equals("VERY POSITIVE")){
                    hashTaggedWordsSentiments.add(1.0);
                }else if (sentiment.toUpperCase().equals("NEUTRAL")){
                    hashTaggedWordsSentiments.add(0.0);
                }
            }
        }
        return hashTaggedWordsSentiments;
    }




}
