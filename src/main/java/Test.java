import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import evaluators.HashTagWordsEvaluator;
import managers.SlangDetector;
import utils.StringUtils;

import java.io.*;
import java.util.*;

/**
 * Created by nane on 3/25/17.
 */
public class Test {
    public static void main(String[] args) {
        String text = "I live in #rome #love #city";
        System.out.println(new HashTagWordsEvaluator().evaluate(text));

    }

    public static String removeAnnotations(String text) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char current = text.charAt(i);
            if (current != '@') {
                sb.append(current);
            } else {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

}
